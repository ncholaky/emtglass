package com.emtglass.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Main demo app for getting started with the OpenTok Android SDK. It contains:
 * - a basic hello-world activity - a basic hello-world activity with control
 * bar with action buttons to switch camera, audio mute and end call. - a basic
 * hello-world activity wwith a customer video capturer out of SDK.
 */
public class MainActivity extends Activity {

	private static final String LOGTAG = "demo-opentok-sdk";
	private HttpClient client;
	private SharedPreferences pref;
	private String SESSION_ID;
	private String TOKEN;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main_activity);
		  if (android.os.Build.VERSION.SDK_INT > 9) {
	            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	            StrictMode.setThreadPolicy(policy);
	            System.out.println("*** My thread is now configured to allow connection");
	        }
		pref = this.getSharedPreferences("com.emtglass.app", Context.MODE_PRIVATE);
		SESSION_ID = pref.getString("com.emtglass.app.sessionid", "SESSION_ID");
		TOKEN = pref.getString("com.emtglass.app.token", "TOKEN");
		final ListView listActivities = (ListView) findViewById(R.id.listview);
		String[] activityNames = {
				getString(R.string.stream),
				
				 };

		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, activityNames);
        listActivities.setAdapter(adapter);

        listActivities.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                    long id) {
                // these positions are hard-coded to some example activities,
                // they match
                // the array contents of activityNames above.
                if (0 == position) {
                    getTokens();
                   // startStream();
                }
                else {
                    Log.wtf(LOGTAG, "unknown item clicked?");
                }
            }
        });
        
        // Disable screen dimming
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume(); 
    }
    
    public void startStream() {

        Log.i(LOGTAG, "starting EMT GLASS");
//        getTokens();
        Intent intent = new Intent(MainActivity.this, UIActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    public void getTokens(){
    	
    	new Thread(new Runnable() {

    	    @Override
    	    public void run() {
		    	String getTokenURL = "http://emtglass.ucsd.edu/to";
		    	
		    	client = new DefaultHttpClient();
		    	HttpGet getrequest = new HttpGet(getTokenURL);		
    			InputStream in = null;
    			char[] buf = new char[5000];
    			try{
    				HttpResponse response = client.execute(getrequest);
    				HttpEntity getRespEn = response.getEntity(); 
    				in = getRespEn.getContent();
    			
    				Reader reader = new InputStreamReader(in);
    				reader.read(buf);

    				String resp = new String(buf);
    				Log.e("RESPONSE",resp.trim());
    					JSONArray respArr = new JSONArray(resp.trim());
    					JSONObject serverResponse = respArr.getJSONObject(0);
    					String sessID = serverResponse.getString("sessionId");
    					String token = serverResponse.getString("token");
    					pref.edit().putString("com.emtglass.app.sessionid", sessID).apply();
    					pref.edit().putString("com.emtglass.app.token", token).apply();
    					startStream();
    					
    			}catch(IOException e){
    				System.out.println(e.getMessage());
    				//return -1;
    			
    			}catch (JSONException e) {
    			// TODO Auto-generated catch block
    				e.printStackTrace();
    				//return -1;
    			}
		
		    }
		}).start();

    }



}