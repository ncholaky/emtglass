package com.emtglass.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;


import com.emtglass.main.R;
import com.emtglass.main.UIActivity;


public class LaunchActivity extends Activity {

    private static final String TAG = LaunchActivity.class.getSimpleName();
	private HttpClient client;
	private SharedPreferences pref;
	private String SESSION_ID;
	private String TOKEN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            System.out.println("*** My thread is now configured to allow connection");
        }
		pref = this.getSharedPreferences("com.emtglass.app", Context.MODE_PRIVATE);
		SESSION_ID = pref.getString("com.emtglass.app.sessionid", "SESSION_ID");
		TOKEN = pref.getString("com.emtglass.app.token", "TOKEN");
        // delayed camera activity
        // see: https://code.google.com/p/google-glass-api/issues/detail?id=259
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                try {
//                    ActivityInfo activityInfo = getPackageManager().getActivityInfo(getComponentName(), 0);
//                    processVoiceAction(activityInfo.loadLabel(getPackageManager()).toString());
//                } catch (NameNotFoundException e) {
//                    e.printStackTrace();
                    processVoiceAction("Connect");
//                }
            }
        }, 100);

    }
 public void getTokens(){
    	
    	new Thread(new Runnable() {

    	    @Override
    	    public void run() {
		    	String getTokenURL =  "http://emtglass.ucsd.edu/to";//"http://server-env-j828umvpmj.elasticbeanstalk.com/to";
		    	
		    	client = new DefaultHttpClient();
		    	HttpGet getrequest = new HttpGet(getTokenURL);		
    			InputStream in = null;
    			char[] buf = new char[5000];
    			try{
    				HttpResponse response = client.execute(getrequest);
    				HttpEntity getRespEn = response.getEntity(); 
    				in = getRespEn.getContent();
    			
    				Reader reader = new InputStreamReader(in);
    				reader.read(buf);

    				String resp = new String(buf);
    				Log.e("RESPONSE",resp.trim());
    					JSONArray respArr = new JSONArray(resp.trim());
    					JSONObject serverResponse = respArr.getJSONObject(0);
    					String sessID = serverResponse.getString("sessionId");
    					String token = serverResponse.getString("token");
    					pref.edit().putString("com.emtglass.app.sessionid", sessID).apply();
    					pref.edit().putString("com.emtglass.app.token", token).apply();
    					startStream();
    					
    			}catch(IOException e){
    				System.out.println(e.getMessage());
    				//return -1;
    			
    			}catch (JSONException e) {
    			// TODO Auto-generated catch block
    				e.printStackTrace();
    				//return -1;
    			}
		
		    }
		}).start();

    }
	 
	 public void startStream() {
	
	    // Log.i(LOGTAG, "starting EMT GLASS");
	//     getTokens();
	     Intent intent = new Intent(LaunchActivity.this, UIActivity.class);
	     intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
	             | Intent.FLAG_ACTIVITY_CLEAR_TOP);
	     startActivity(intent);
	 }

    private void processVoiceAction(String command) {
        Log.v(TAG, "Voice command: " + command);
//        if (command == null) {
//            startActivity(CaptureActivity.newIntent(this, ScanAction.UNDEFINED));
//        } else if (command.toLowerCase().contains("product")) {
            //startActivity(CaptureActivity.newIntent(this));
          getTokens();
         // Intent intent = new Intent(this, com.emtglass.main.MainActivity.class);
         // startActivity(intent);
//        } else {
//            startActivity(CaptureActivity.newIntent(this, ScanAction.UNDEFINED));
//        }

        finish();
    }
}
