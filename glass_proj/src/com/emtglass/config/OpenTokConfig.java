package com.emtglass.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class OpenTokConfig extends Application  {
	
    private static OpenTokConfig instance;
    SharedPreferences pref = this.getSharedPreferences("com.emtglass.app", Context.MODE_PRIVATE);
  //Log.i("THIS IS IT: ", res);
  	// *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
  	// ***                      https://dashboard.tokbox.com/projects                           ***
  	// Replace with a generated Session ID
  		//CALL DB TO GET GENERATED SESSION ID AND TOKEN -N
  	
      public String SESSION_ID = pref.getString("com.emtglass.app.sessionid", "SESSION_ID");
  	// Replace with a generated token (from the dashboard or using an OpenTok server SDK)
  	public String TOKEN = pref.getString("com.emtglass.app.token", "TOKEN"); 
  	// Replace with your OpenTok API key
  	public final String API_KEY= "45011202";
  	
  	
  	// Subscribe to a stream published by this client. Set to false to subscribe
      // to other clients' streams only.
      public static final boolean SUBSCRIBE_TO_SELF = false;
     // private HttpClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
      
    	SESSION_ID = pref.getString("com.emtglass.app.sessionid", "SESSION_ID");
    	TOKEN = pref.getString("com.emtglass.app.token", "TOKEN");
    	
    }

    public static OpenTokConfig getInstance() {
        return instance;
    }
	
}