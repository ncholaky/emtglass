var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tokenSchema = new Schema({
  name: String,
  token: String,
  sessionId: String
});

mongoose.model('tokens', tokenSchema);
