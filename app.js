// Dependencies

var express = require('express'),
	mongoose = require('mongoose'),
	stringifyObject = require('stringify-object'),
	Cryptos = require('crypto'),
    OpenTok = require('lib/opentok');
	
var tok, sess; 


// Verify that the API Key and API Secret are defined
var apiKey = "45126802" , //process.env.API_KEY,
    apiSecret = "58c667db80bd062f68c7b6079d937db74ab70e5d" ; //add to environment later //process.env.API_SECRET; 
	//these variables will be read from the Elastic Beanstalk environment variables
	
var path = "emtglassresearch/" + apiKey + "/"; //"elasticbeanstalk-us-west-1-709078457089/" +
	

var archiveId;
	
	
if (!apiKey || !apiSecret) {
  console.log(new Date+'-You must specify API_KEY and API_SECRET environment variables');
  process.exit(1);
}

// Initialize the express app
var app = express();
app.use(express.static(__dirname + '/public'));
app.use( express.static( "public" ) );
// Initialize OpenTok
var opentok = new OpenTok(apiKey, apiSecret);


// Create a session and store it in the express app
opentok.createSession({mediaMode:"routed"},function(err, session) {
  if (err) throw err;
  app.set('sessionId', session.sessionId);
  // We will wait on starting the app until this is done
  init();
});

	var tokenSchema =  mongoose.Schema({
                          device: String
                        , token: String
                        , sessionId: String
                        , savedOn: String
                        });

var sessionId;
mongoose.connect('mongodb://132.239.95.35:2015/emtglass'); //establishing connection to the MongoDB Database currently running on ucsd server
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function callback()
{
	console.log(new Date+"-CONNECtion");
	var Token = mongoose.model('tokens', tokenSchema);
    app.get('/', function(req, res) {
  	sessionId = app.get('sessionId'),
    // generate a fresh token for this client
    token = opentok.generateToken(sessionId);
	tok = token;
	//console.log("Token is: ", tok);
	sess = sessionId;
    var obj = {"device": "glass", "token": token, "sessionId": sessionId, "savedOn": new Date()}; 
	Token.update({device: "glass"} , obj, function(err, numAffected) {
	console.log(new Date+"-We have entered this place");
	if(err) console.log(new Date+"-There has been an error: " + err);
		console.log(new Date+"-No Error, move ON: \n");
		if(numAffected === 0){
			var tok = new Token({
				device: 'glass'
				,	token: token
				,	sessionId: sessionId
				, 	savedOn: new Date()
				});
			console.log(new Date+"-Created tok!!! \n");
			tok.save(function(err,doc){
			if(err) console.log(new Date+"-save "+ err);
					console.log(new Date+"-HEY WE HAVE CREATED A NEW TOKEN"); 
				});
					
				console.log(new Date+"-Number of token models updated: "+numAffected);
				}
			});			
			/////
			res.render('index.ejs', {
				apiKey: apiKey,
				sessionId: sessionId,
				token: token
			});
		});
		
		
		
	 /////////DB end
//handling start archiving GET request from the server
app.get('/start', function(req, res) {
	console.log(new Date+"-inside start what's going on? ? " );
    opentok.startArchive(sessionId, {
    name: 'New Archive'
  }, function(err, archive) {
    if (err) return res.send(500,
      'Could not start archive for session '+sessionId+'. error='+err.message
    );
	console.log(new Date+"-archive id is: " + archive.id);
    //console.log(new Date+"-Please work, this is taking a while... :( ");
	console.log(new Date+"-result from archive: " + archive);
  });
});

//handling start archiving GET request from the server
		app.get('/stop/:archiveId', function(req, res) {
		  archiveId = req.param('archiveId');	
		  path = path + (archiveId + "/archive.mp4"); 
		  		var http  = require('http')
		  , https = require('https')
		  , aws4  = require('aws4')
		var host = 's3-us-west-2.amazonaws.com'; //elasticbeanstalk-us-west-1-709078457089.
		var preSignedURL = 'https://' + host + '/' + path + '/?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=';
		// given an options object you could pass to http.request
		var opts = { host: host, path: path }

		aws4.sign(opts) // assumes AWS credentials are available in process.env

		console.log(opts);
		var opt = stringifyObject(opts);
		//console.log("WHAT: " + opt);
		var sepEq = opt.split("=");
		var dateArr = sepEq[0].split("'X-Amz-Date': '");
		var dateA = dateArr[1].split("'");
		var date = dateA[0];
		console.log("Date is: " + date);
		var credArr = sepEq[1].split(",");
		var credential = credArr[0];
		credential = credential.replace(/\//g, "%2F");
		var signature = sepEq[3].split("'")[0];
		console.log("cred: " + credential);
		console.log("sign: " + signature);
		preSignedURL = preSignedURL + credential + "&X-Amz-Date=" + date + "&X-Amz-Expires=86400&X-Amz-SignedHeaders=host&X-Amz-Signature=" + signature ; 
		console.log("preSignedURL: " + preSignedURL.trim());  
		  opentok.stopArchive(archiveId, function(err, archive) {
			if (err) return res.send(500, 'Could not stop archive '+archiveId+'. error='+err.message);
			res.json(archive);
		
		  });		 
		});

//this request is made from the android application to access the current sessionID to connect to
	       	app.get('/to', function(req, res) {
			//  console.log("HEY we are in the tokens place, yo");
			  mongoose.model('tokens').find(function(err, tokens) {
				if(err)console.log(err);
				res.send(tokens);
				console.log(new Date+"-getting token and session id");
			  });
			});
});

// Start the express app
function init() {
  app.set('port', process.env.PORT || 3000);
  app.listen(app.get('port'), function() {
    console.log(new Date+'-EmtGlass is now running');
  });
}
